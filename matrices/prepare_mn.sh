#!/bin/bash

if ! grep "source ~/.modules_mn" ~/.bashrc
then
  cat  >> ~/.bashrc <<EOF

# ***
source ~/.modules_mn
# ***

EOF

fi

cat > ~/.modules_mn <<EOF
ml gcc/8.1.0

export COMPSS_PYTHON_VERSION=3
module load COMPSs/2.5

ml mkl/2018.1
ml impi/2018.1
ml opencv/4.1.2
ml python/3.6.4_ML
ml DATACLAY/2.0rc
EOF
