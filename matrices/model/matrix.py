import numpy as np

# Real PyCOMPSs imports are available from within PyCOMPSs executions
from dataclay.contrib.dummy_pycompss import task, INOUT, IN, CONCURRENT

from dataclay import DataClayObject, dclayMethod


class Block(DataClayObject):
    """
    @dclayImport numpy as np

    @ClassField submatrix numpy.ndarray
    """
    @dclayMethod(submatrix="numpy.ndarray")
    def __init__(self, submatrix):
        self.submatrix = submatrix


class Matrix(DataClayObject):
    """
    @dclayImport numpy as np

    # Total number of blocks is this number squared
    @ClassField num_blocks int
    # The size of a block (size of the block, which also is a square matrix)
    @ClassField blocksize int
    # The total side size of the matrix
    @ClassField size int
    # Internally, store the blocks in a two-dimensional structure
    @ClassField blocks list<list<model.matrix.Block>>
    """

    @dclayMethod(n="int")
    def __init__(self, n):
        # Just for demonstration purposes, let's hardcode 1024 as the blocksize
        self.blocksize = 1024
        self.num_blocks = n // 1024
        self.size = n

        # Start uninitialized
        self.blocks = list()
    
    @dclayMethod()
    def random(self):
        for i in range(self.num_blocks):
            row = list()
            self.blocks.append(row)
            for j in range(self.num_blocks):
                row.append(Block(np.random.random((self.blocksize, self.blocksize))))

    @dclayMethod()
    def zeros(self):
        for i in range(self.num_blocks):
            row = list()
            self.blocks.append(row)
            for j in range(self.num_blocks):
                row.append(Block(np.zeros((self.blocksize, self.blocksize))))
