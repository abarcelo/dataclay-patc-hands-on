#!/bin/bash

cat > $PWD/execution_values << EOF
BACKENDS_PER_NODE=8
EOF

enqueue_compss \
	--reservation=PATC20-DATA \
	--num_nodes=2 \
	--exec_time=30 \
	--job_name=patc_matmul \
	--max_tasks_per_node=16 \
	--worker_in_master_cpus=0 \
	--summary \
	--graph \
	--lang=python \
	--classpath=$DATACLAY_JAR:$DATACLAY_DEPENDENCY_LIBS/* \
	--pythonpath=$PWD \
	--storage_home=$COMPSS_STORAGE_HOME \
	--storage_props="$PWD/execution_values" \
	--prolog="./prepare_storage_mn.sh" \
		$PWD/matmul_main.py

