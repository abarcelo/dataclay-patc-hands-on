#!/usr/bin/env python3

# This file should be used with PyCOMPSs
# (typically, through an enqueue_compss command)

from model.matrix import Matrix, Block

if __name__ == "__main__":
    # Random generation of 4096 x 4096 matrices
    a = Matrix(4096)
    b = Matrix(4096)
    # Make them persistent
    a.make_persistent()
    b.make_persistent()
    # Initialize the two matrices with random numbers
    a.random()
    b.random()

    print("Ready to start the multiplication")
    c = a @ b
    print("Multiplication finished")
