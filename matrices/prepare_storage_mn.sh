#!/bin/bash

EXECUTION_DIR=~/.COMPSs/$SLURM_JOB_ID/storage
export DATACLAYCLIENTCONFIG=$EXECUTION_DIR/cfgfiles/client.properties
STUBS_PATH=$EXECUTION_DIR/stubs
APP_PATH=~/matrices
DATASET=hpc_dataset
USERNAME=bsc_user
PASSWORD=bsc_user

echo "*** Predefined variables in the environment: ***"
echo "CLASSPATH=$CLASSPATH"
echo "DATACLAY_TOOL=$DATACLAY_TOOL"
echo "************************************************"
echo
echo "***"
echo "*** Creating account \`${USERNAME}\`"
echo "***"
$DATACLAY_TOOL NewAccount $USERNAME $PASSWORD

echo
echo "***"
echo "*** Creating a new DataContract called \`${DATASET}\`"
echo "***"
$DATACLAY_TOOL NewDataContract $USERNAME $PASSWORD $DATASET $USERNAME

echo
echo "***"
echo "*** Registering all the data model \`model\` in \`./model\`"
echo "***"
$DATACLAY_TOOL NewModel $USERNAME $PASSWORD model ./model python

echo
echo "***"
echo "*** Getting stubs and putting them in stubs' folder"
echo "***"
rm -rf ./stubs
mkdir -p ./stubs
$DATACLAY_TOOL GetStubs $USERNAME $PASSWORD model $STUBS_PATH
