#!/bin/bash

DATASET=DemoDS
USERNAME=DemoUser
PASSWORD=DemoPass

export CLASSPATH=/opt/dataclay-2.0-jar-with-dependencies.jar:$CLASSPATH
export DATACLAY_TOOL=/opt/dataclaycmd.sh

echo "*** Predefined variables in the environment: ***"
echo "CLASSPATH=$CLASSPATH"
echo "DATACLAY_TOOL=$DATACLAY_TOOL"
echo "************************************************"
echo
echo "***"
echo "*** Creating account \`${USERNAME}\`"
echo "***"
$DATACLAY_TOOL NewAccount $USERNAME $PASSWORD

echo
echo "***"
echo "*** Creating a new DataContract called \`${DATASET}\`"
echo "***"
$DATACLAY_TOOL NewDataContract $USERNAME $PASSWORD $DATASET $USERNAME

echo
echo "***"
echo "*** Registering all the data model \`model\` in \`./stubs\`"
echo "***"
$DATACLAY_TOOL NewModel $USERNAME $PASSWORD model ./model python

echo
echo "***"
echo "*** Getting stubs and putting them in ./stubs"
echo "***"
rm -rf ./stubs
mkdir -p ./stubs
$DATACLAY_TOOL GetStubs $USERNAME $PASSWORD model ./stubs
